package testMaven;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class FirstClass {
	public static WebDriver d;
	public static ExtentReports er;
	public static ExtentTest logger;
	
	@BeforeSuite(alwaysRun = true)
	public void test1(){
		System.out.println("execution started");
		er = new ExtentReports(System.getProperty("user.dir")+"//test-output//ExtentReportResult.html", true);
		er
						.addSystemInfo("Host Name", "Rightship")
		                .addSystemInfo("Environment", "UAT")
		                .addSystemInfo("URL","https://uatqi.rightship.com/a4");
		                //.addSystemInfo("User Name", "Hima Bindu K");

		                er.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}
	@Test(groups={"invoke"},priority=0)
	public static void chromeDriver(){
		
		System.setProperty("webdriver.chrome.driver", "D:\\POC\\testMaven\\testMaven\\Resources\\chromedriver.exe");
		logger = er.startTest("Chrome Driver");
		d = new ChromeDriver();
		logger.log(LogStatus.INFO, "Initilised Chrome driver");
		d.get("https://www.google.com");
		logger.log(LogStatus.INFO, "Launched google");
		if((d.getTitle()).contains("Google")){
			logger.log(LogStatus.PASS, "Google browser is launched as expected.");
		}else{
			logger.log(LogStatus.PASS, "Google browser is not launched as expected.");
		}	
	}
	@Test(groups={"invoke"},priority=1)
	public static void googleSearch(){
		logger = er.startTest("google Search");
		d.findElement(By.id("lst-ib")).sendKeys("Bamboo");
		logger.log(LogStatus.INFO, "Bamboo text is entered in search box.");
		d.findElement(By.name("btnK")).click();
		List<WebElement> links = d.findElements(By.tagName("a"));
		for(WebElement e:links){
			if(e.getText().contains("Wikipedia")){
				logger.log(LogStatus.PASS, "Bamboo Wiki link is displayed.");
				e.click();
				break;
			}
		}
	}
	
	@AfterMethod(alwaysRun=true)
	public void getResult(ITestResult tr) {
		if(tr.getStatus() == ITestResult.SUCCESS){
			logger.log(LogStatus.PASS, tr.getName() + "Test Case is Passed" );
		}
	else if(tr.getStatus() == ITestResult.FAILURE){
		 logger.log(LogStatus.FAIL, "Test Case '"+ tr.getName() +"' is Failed");
	 }else if(tr.getStatus() == ITestResult.SKIP){
		 logger.log(LogStatus.SKIP, "Test Case Skipped is "+tr.getName());
	 }
	 // ending test
	 //endTest(logger) : It ends the current test and prepares to create HTML report
	 er.endTest(logger);
	 }		
	@AfterSuite(alwaysRun=true)
	public void tearDown(){
		er.flush();
		d.quit();
	}
}
